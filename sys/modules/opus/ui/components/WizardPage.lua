local class = require("eos.class")
local UI = require("eos.ui")

UI.WizardPage = class(UI.Window)
UI.WizardPage.defaults = {
    UIElement = "WizardPage",
    ey = -2,
}
function UI.WizardPage.validate()
    return true
end
