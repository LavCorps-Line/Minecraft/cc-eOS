local class = require("eos.class")
local UI = require("eos.ui")

UI.MenuItem = class(UI.FlatButton)
UI.MenuItem.defaults = {
    UIElement = "MenuItem",
    noPadding = false,
    textInactiveColor = "gray",
}
