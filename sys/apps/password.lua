local Security = require("eos.security")
local SHA = require("eos.crypto.sha2")
local Terminal = require("eos.terminal")

local password = Terminal.readPassword("Enter new password: ")

if password then
    Security.updatePassword(SHA.compute(password))
    print("Password updated")
end
